﻿using UnityEngine;

public class Paddle : MonoBehaviour {

    [SerializeField]
    float screenWidthInUnits = 16f;
    [SerializeField]
    float min = 1f;
    [SerializeField]
    float max = 15f;

    void Update()
    {
        var mousePosInFrame = Input.mousePosition.x / Screen.width * screenWidthInUnits;
        var xPosition = Mathf.Clamp(mousePosInFrame, min, max);
        transform.position = new Vector2(xPosition, transform.position.y);
    }

}
