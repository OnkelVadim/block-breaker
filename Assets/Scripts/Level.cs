﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour {

    [SerializeField] int breakableBlockCount;

    SceneLoader sceneLoader;

    private void Start()
    {
        sceneLoader = FindObjectOfType<SceneLoader>();
    }
    public void CountBreakableBloks()
    {
        breakableBlockCount++;
    }

    public void BlockDestroyed()
    {
        breakableBlockCount--;
        if(breakableBlockCount <= 0)
        {
            sceneLoader.LoadNextScene();
        }
    }
}
