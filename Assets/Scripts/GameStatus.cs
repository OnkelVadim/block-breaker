﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameStatus : MonoBehaviour {

    [Range(0.1f,7f)][SerializeField] float gameSpeed = 1f;
    [SerializeField] int scorePerBlock = 100;
    [SerializeField] int gameScore = 0;
    [SerializeField] TextMeshProUGUI scoreText;

    private void Awake()
    {
        int objectCount = FindObjectsOfType<GameStatus>().Length;
        if(objectCount>1)
        {
            gameObject.SetActive(false);
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);
    }
    private void Start()
    {
        scoreText.text = gameScore.ToString();
    }
    void Update () {
        Time.timeScale = gameSpeed;
	}

    public void AddToScore()
    {
        gameScore += scorePerBlock;
        scoreText.text = gameScore.ToString();
    }

    public void Destroy()
    {
        Destroy(gameObject);
    }
}
