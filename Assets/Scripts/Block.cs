﻿using System;
using UnityEngine;

public class Block : MonoBehaviour
{

    [SerializeField] GameObject destroyVFX;
    [SerializeField] AudioClip blockSound;
    [SerializeField] Sprite[] hitSprites;
    [SerializeField] int maxHits = 0;

    SpriteRenderer spriteRenderer;
    GameStatus gameStatus;
    Level level;
    int hitsCounter = 0;

    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        level = FindObjectOfType<Level>();
        if (tag == "Breakable")
        {
            level.CountBreakableBloks();
        }
        gameStatus = FindObjectOfType<GameStatus>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        AudioSource.PlayClipAtPoint(blockSound, transform.position);

        if (tag != "Breakable")
        {
            return;
        }

        if (maxHits <= ++hitsCounter)
        {
            DestroyBlock();
        }

        ShowNextHitSprite();
    }

    private void ShowNextHitSprite()
    {
        if(hitSprites == null)
        {
            return;
        }

        var spriteIndex = hitsCounter - 1;

        if(spriteIndex == hitSprites.Length)
        {
            return;
        }

        spriteRenderer.sprite = hitSprites[spriteIndex];
    }

    private void DestroyBlock()
    {
        level.BlockDestroyed();
        gameStatus.AddToScore();
        TriggerVFX();
        Destroy(gameObject);
    }

    private void TriggerVFX()
    {
        var vfx = Instantiate(destroyVFX, transform.position, transform.rotation);
        Destroy(vfx, 1f);
    }
}
